const express = require('express'),
    passport = require('passport'),
    auth = require('../middlewares/auth'),
    UserController = require('../controllers/userController.js')

const router = express.Router();

router.post('/signup', [function(req, res, next) {
    passport.authenticate('signup', {
      session: false
    }, function(err, user, info) {
      if (err) {
        return next(err);
      }
  
      if (!user) {
        res.status(401).json({
          status: 'Error',
          message: info.message
        });
        return;
      }
  
      UserController.signup(user, req, res);
    })(req, res, next);
}]);

router.post('/signin', [function(req, res, next) {
    passport.authenticate('login', {
      session: false
    }, function(err, user, info) {
      if (err) {
        return next(err);
      }
  
      if (!user) {
        res.status(401).json({
          status: 'Error',
          message: info.message
        });
        return;
      }
  
      UserController.login(user, req, res);
    })(req, res, next);
}]);

router.get('/', [passport.authenticate('authorization', {
    session: false
})], UserController.getAll);

module.exports = router;