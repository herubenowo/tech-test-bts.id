const { shopping } = require('../models');

class ShoppingController {
    async create(req, res) {
        try {
            const createShopping = await shopping.create({
                name: req.body.name,
                createdDate: req.body.createdDate
            })

            const newShopping = await shopping.findOne({
                where: {
                    id: createShopping.id
                },
                attributes: ['id', 'name', 'createdDate']
            })

            return res.status(200).json({
                data: newShopping
            })
        } catch (e) {
            return res.status(422).json({
                status: `Error!`
            })
        };
    };

    async update(req, res) {
        try {
            const body = {
                name: req.body.name,
                createdDate: req.body.createdDate
            }

            const updateShopping = await shopping.update(body, {
                where: {
                    id: req.params.id
                }
            })

            const updatedShopping = await shopping.findOne({
                where: {
                    id: req.params.id
                },
                attributes: ['id', 'name', 'createdDate']
            })

            return res.status(200).json({
                data: updatedShopping
            })
        } catch (e) {
            return res.status(422).sjon({
                status: `Error!`
            })
        }
    }

    async getAll(req, res) {
        try {
            const findShopping = await shopping.findAll({
                attributes: ['id', 'name', 'createdDate']
            })

            return res.status(200).json({
                data: findShopping
            })
        } catch (e) {
            return res.status(422).json({
                status: `Error!`
            })
        }
    }

    async getOne(req, res) {
        try {
            const findOne = await shopping.findOne({
                where: {
                    id: req.params.id
                },
                attributes: ['id', 'name', 'createdDate']
            })

            return res.status(200).json({
                data: findOne
            })
        } catch (e) {
            return res.statsu(422).json({
                status: `Error!`
            })
        }
    }

    async delete(req, res) {
        try {
            const deleteShopping = await shopping.destroy({
                where: {
                    id: req.params.id
                }
            })

            return res.status(200).json({
                status: `Success!`
            })
        } catch (e) {
            return res.status(422).json({
                status: `Error!`
            })
        }
    }
}

module.exports = new ShoppingController