const express = require('express'),
    bodyParser = require('body-parser'),
    cors = require('cors'),
    userRoutes = require('./routes/userRoutes.js'),
    shoppingRoutes = require('./routes/shoppingRoutes.js');

const app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cors());

app.use('/api/users', userRoutes);
app.use('/api/shopping', shoppingRoutes);

app.listen(4000, () => console.log(`Server running on http://localhost:4000`));