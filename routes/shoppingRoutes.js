const express = require('express'),
    passport = require('passport'),
    auth = require('../middlewares/auth'),
    ShoppingController = require('../controllers/shoppingController.js')

const router = express.Router();

router.post('/create', [passport.authenticate('authorization', {
    session: false
})], ShoppingController.create);

router.put('/update/:id', [passport.authenticate('authorization', {
    session: false
})], ShoppingController.update);

router.get('/', [passport.authenticate('authorization', {
    session: false
})], ShoppingController.getAll);

router.get ('/get/:id', [passport.authenticate('authorization', {
    session: false
})], ShoppingController.getOne)

router.delete('/delete/:id', [passport.authenticate('authorization', {
    session: false
})], ShoppingController.delete);

module.exports = router;