const { user } = require('../models');
const passport = require('passport');
const jwt = require('jsonwebtoken');

class UserController {
    async signup(user, req, res) {
        try {
            const body = {
                id: user.id,
                email: user.email
            };

            const token = jwt.sign({
                user: body
            }, 'secret_password');

            return res.status(200).json({
                username: user.username,
                email: user.email,
                token: token
            });
        } catch (e) {
            return res.status(401).json({
                status: 'Error!',
                message: e
            });
        };
    };

    async login(user, req, res) {
        try {
            const body = {
                id: user.id,
                email: user.email
            };

            const token = jwt.sign({
                user: body
            }, 'secret_password');

            return res.status(200).json({
                username: user.username,
                email: user.email,
                token: token
            });
        } catch (e) {
            return res.status(401).json({
                status: 'Error!',
                message: e
            });
        };
    };

    async getAll(req, res) {
        try {   
            const findAllUser = await user.findAll({
                attributes: ['id','name', 'email', 'username', 'address', 'phone', 'country', 'city', 'postcode', 'createdAt'],
                order: [['createdAt', 'ASC']]
            });

            return res.status(200).json({
                message: `Success!`,
                data: findAllUser
            });
        } catch (e) {
            return res.status(422).json({
                status: `Error!`,
                errors: e
            });
        };
    };
}

module.exports = new UserController;