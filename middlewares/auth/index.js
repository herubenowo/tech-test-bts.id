const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const {
    user
} = require('../../models');
const bcrypt = require('bcrypt');
const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;

passport.use(
    'signup',
    new localStrategy({
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true
        },
        async (req, email, password, done) => {
            try {
                let createUser = await user.create({
                    email: email,
                    password: password,
                    name: req.body.name,
                    username: req.body.username,
                    address: req.body.address,
                    phone: req.body.phone,
                    country: req.body.country,
                    city: req.body.city,
                    postcode: req.body.postcode
                });

                let newUser = await user.findOne({
                    where: {
                        id: createUser.id
                    },
                    attributes: ['id', 'email', 'username']
                });

                return done(null, newUser, {
                    message: "Signup success!"
                });
            } catch (e) {
                return done(null, false, {
                    message: "User can't be created!"
                })
            }
        }
    )
);

passport.use(
    'login',
    new localStrategy({
            usernameField: 'email',
            passwordField: 'password'
        },
        async (email, password, done) => {
            try {
                const userLogin = await user.findOne({
                    where: {
                        email: email
                    }
                });

                if (!userLogin) {
                    return done(null, false, {
                        message: 'Email not found!'
                    })
                };

                const validate = await bcrypt.compare(password, userLogin.password);

                if (!validate) {
                    return done(null, false, {
                        message: 'You put the wrong password!'
                    })
                };

                let userLoginVisible = await user.findOne({
                    where: {
                        email: email
                    },
                    attributes: ['id', 'email', 'username']
                });

                return done(null, userLoginVisible, {
                    message: 'Login success!'
                });
            } catch (e) {
                return done(null, false, {
                    message: "Can't login!"
                })
            }
        }
    )
);

passport.use(
    'authorization',
    new JWTstrategy({
            secretOrKey: 'secret_password',
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken()
        },
        async (token, done) => {
            try {
                const userLogin = await user.findOne({
                    where: {
                        id: token.user.id
                    },
                    attributes: ['id','name', 'email', 'username', 'address', 'phone', 'country', 'city', 'postcode', 'createdAt']
                });

                if (!userLogin) {
                    return done(null, false, {
                        message: "Email not found!"
                    })
                };

                return done(null, userLogin, {
                    message: "Authorized!"
                });
            } catch (e) {
                return done(null, false, {
                    message: "Unauthorized!"
                });
            }
        }
    )
);