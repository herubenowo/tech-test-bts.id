# Heru Benowo
# heru.benowo@yahoo.co.id / heru.benowo@gmail.com
# Server running on localhost:4000
# Before running the app please type (For installing all the node_modules) : npm install 
# For running the app please type: npm test
# For testing please use body (x-www-form-urlencoded)
# - User Signup
# POST localhost:4000/api/users/signup
# - User Signin
# POST localhost:4000/api/users/signin
# - User Get all (use Bearer token)
# GET localhost:4000/api/users/
# - Shopping Create (use Bearer token)
# POST localhost:4000/api/shopping/create
# - Shopping Update (use Bearer token)
# PUT localhost:4000/api/shopping/update/:id
# - Shopping Get All (use Bearer token)
# GET localhost:4000/api/shopping/
# - Shopping Get by id (use Bearer token)
# GET localhost:4000/api/shopping/get/:id
# - Shopping Delete (use Bearer token)
# DELETE localhost:4000/api/delete/:id